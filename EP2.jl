#Nome: Eike Souza da Silva NUSP: 4618653
#EP2 - MAC0110 2020

function checkWill(cards)
    kaprekar = 6147
    aux = kaprekar
    j = 0
    contadigito = 0

    while aux > 0
        aux = div(aux, 10)
        contadigito = contadigito + 1
    end

    while kaprekar > 0
        for i in 1: length(cards)
            if cards[i] == kaprekar % 10
                j = j + 1
                splice!(cards, i)
                break
            end
        end
        kaprekar = div(kaprekar, 10)
    end

    if j == contadigito
        return true
    else
        return false
    end
end

function checkTaki(cards)
    numUm = 0
    numSete = 0

    for i in 1: length(cards)
        if cards[i] == 1
            numUm = numUm + 1
        end

        if cards[i] == 7
            numSete = numSete + 1
        end
    end

    if numUm >= 2 && numSete >= 2 && checkWill(cards) == true
        return true
    else
        return false
    end
end

function checkJackson(x, cards)
    contadigito = 0
    aux = x
    j = 0

    while aux > 0
        aux = div(aux, 10)
        contadigito = contadigito + 1
    end

    while x > 0
        for i in 1: length(cards)
            if cards[i] == x % 10
                j = j + 1
                splice!(cards, i)
                break
            end
        end
        x = div(x, 10)
    end

    if checkTaki(cards) == true && j == contadigito
        return true
    else
        return false
    end
end

function checkWillBase(b, cards)
    favwill = 6174
    novabase = []
    k = 0

    while favwill > 0
        push!(novabase, favwill % b)
        favwill = div(favwill,b)
    end
    n = length(novabase)

    for i in 1:length(novabase)
        for j in 1:length(cards)
            if novabase[i] == cards[j]
                k = k + 1
                splice!(cards, j)
                break
            end
        end
    end

    if k == n
        return true
    else
        return false
    end
end







